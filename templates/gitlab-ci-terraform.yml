# =========================================================================================
# Copyright (C) 2021 Orange & contributors
#
# This program is free software; you can redistribute it and/or modify it under the terms
# of the GNU Lesser General Public License as published by the Free Software Foundation;
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along with this
# program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA  02110-1301, USA.
# =========================================================================================
# default workflow rules: Merge Request pipelines
workflow:
  rules:
    # prevent branch pipeline when an MR is open (prefer MR pipeline)
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - when: always

# test job prototype: implement adaptive pipeline rules
.test-policy:
  rules:
    # on tag: auto & failing
    - if: $CI_COMMIT_TAG
    # on ADAPTIVE_PIPELINE_DISABLED: auto & failing
    - if: '$ADAPTIVE_PIPELINE_DISABLED == "true"'
    # on production or integration branch(es): auto & failing
    - if: '$CI_COMMIT_REF_NAME =~ $PROD_REF || $CI_COMMIT_REF_NAME =~ $INTEG_REF'
    # early stage (dev branch, no MR): manual & non-failing
    - if: '$CI_MERGE_REQUEST_ID == null && $CI_OPEN_MERGE_REQUESTS == null'
      when: manual
      allow_failure: true
    # Draft MR: auto & non-failing
    - if: '$CI_MERGE_REQUEST_TITLE =~ /^Draft:.*/'
      allow_failure: true
    # else (Ready MR): auto & failing
    - when: on_success

variables:
  # variabilized tracking image
  TBC_TRACKING_IMAGE: "$CI_REGISTRY/to-be-continuous/tools/tracking:master"

  # Default Docker image (can be overridden)
  TF_IMAGE: "hashicorp/terraform:light"
  TF_TFSEC_IMAGE: "tfsec/tfsec-ci"
  TF_TFSEC_ARGS: "."
  TF_TFLINT_IMAGE: "ghcr.io/terraform-linters/tflint-bundle:latest"
  TF_TFLINT_ARGS: "--enable-plugin=google --enable-plugin=azurerm --enable-plugin=aws"
  TF_CHECKOV_IMAGE: "bridgecrew/checkov"
  TF_CHECKOV_ARGS: "--framework terraform --directory ."
  TF_INFRACOST_IMAGE: "infracost/infracost"
  TF_INFRACOST_ARGS: "breakdown"
  TF_INFACOST_USAGE_FILE: "infracost-usage.yml"
  TF_FMT_ARGS: "-diff -recursive"

  # root module directory
  TF_PROJECT_DIR: "."
  TF_SCRIPTS_DIR: "."
  TF_OUTPUT_DIR: "tf-output"

  # separated tf plan is enabled for production by default
  TF_PROD_PLAN_ENABLED: "true"

  # default production ref name (pattern)
  PROD_REF: '/^(master|main)$/'
  # default integration ref name (pattern)
  INTEG_REF: '/^develop$/'

# allowed stages depend on your template type (see: https://to-be-continuous.gitlab.io/doc/understand/#generic-pipeline-stages)
stages:
  - build
  - test
  - infra
  - infra-prod

.tf-scripts: &tf-scripts |
  # BEGSCRIPT
  set -e

  function log_info() {
      echo -e "[\\e[1;94mINFO\\e[0m] $*"
  }

  function log_warn() {
      echo -e "[\\e[1;93mWARN\\e[0m] $*"
  }

  function log_error() {
      echo -e "[\\e[1;91mERROR\\e[0m] $*"
  }

  function fail() {
    log_error "$*"
    exit 1
  }

  function assert_defined() {
    if [[ -z "$1" ]]
    then
      log_error "$2"
      exit 1
    fi
  }

  function install_ca_certs() {
    certs=$1
    if [[ -z "$certs" ]]
    then
      return
    fi

    # List of typical bundles
    bundles="/etc/ssl/certs/ca-certificates.crt"                            # Debian/Ubuntu/Gentoo etc.
    bundles="${bundles} /etc/ssl/cert.pem"                                  # Alpine Linux
    bundles="${bundles} /etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem"  # CentOS/RHEL 7
    bundles="${bundles} /etc/pki/tls/certs/ca-bundle.crt"                   # Fedora/RHEL 6
    bundles="${bundles} /etc/ssl/ca-bundle.pem"                             # OpenSUSE
    bundles="${bundles} /etc/pki/tls/cacert.pem"                            # OpenELEC

    # Try to find the right bundle to update it with custom CA certificates
    for bundle in ${bundles}
    do
      # import if bundle exists
      if [[ -f "${bundle}" ]]
      then
        # Import certificates in bundle
        echo "${certs}" | tr -d '\r' >> "${bundle}"

        log_info "Custom CA certificates imported in \\e[33;1m${bundle}\\e[0m"
        ca_imported=1
        break
      fi
    done

    if [[ -z "$ca_imported" ]]
    then
      log_warn "Could not import custom CA certificates !"
    fi
  }

  function envsubst_cli() {
    awk '{while(match($0,"[@]{[^}]*}")) {var=substr($0,RSTART+2,RLENGTH -3);gsub("[@]{"var"}",ENVIRON[var])}}1'
  }

  function unscope_variables() {
    _scoped_vars=$(env | awk -F '=' "/^scoped__[a-zA-Z0-9_]+=/ {print \$1}" | sort)
    if [[ -z "$_scoped_vars" ]]; then return; fi
    log_info "Processing scoped variables..."
    for _scoped_var in $_scoped_vars
    do
      _fields=${_scoped_var//__/:}
      _condition=$(echo "$_fields" | cut -d: -f3)
      case "$_condition" in
      if) _not="";;
      ifnot) _not=1;;
      *)
        log_warn "... unrecognized condition \\e[1;91m$_condition\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
      ;;
      esac
      _target_var=$(echo "$_fields" | cut -d: -f2)
      _cond_var=$(echo "$_fields" | cut -d: -f4)
      _cond_val=$(eval echo "\$${_cond_var}")
      _test_op=$(echo "$_fields" | cut -d: -f5)
      case "$_test_op" in
      defined)
        if [[ -z "$_not" ]] && [[ -z "$_cond_val" ]]; then continue;
        elif [[ "$_not" ]] && [[ "$_cond_val" ]]; then continue;
        fi
        ;;
      equals|startswith|endswith|contains|in|equals_ic|startswith_ic|endswith_ic|contains_ic|in_ic)
        # comparison operator
        # sluggify actual value
        _cond_val=$(echo "$_cond_val" | tr '[:punct:]' '_')
        # retrieve comparison value
        _cmp_val_prefix="scoped__${_target_var}__${_condition}__${_cond_var}__${_test_op}__"
        _cmp_val=${_scoped_var#"$_cmp_val_prefix"}
        # manage 'ignore case'
        if [[ "$_test_op" == *_ic ]]
        then
          # lowercase everything
          _cond_val=$(echo "$_cond_val" | tr '[:upper:]' '[:lower:]')
          _cmp_val=$(echo "$_cmp_val" | tr '[:upper:]' '[:lower:]')
        fi
        case "$_test_op" in
        equals*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val" ]]; then continue;
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val" ]]; then continue;
          fi
          ;;
        startswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val"* ]]; then continue;
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val"* ]]; then continue;
          fi
          ;;
        endswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val" ]]; then continue;
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val" ]]; then continue;
          fi
          ;;
        contains*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val"* ]]; then continue;
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val"* ]]; then continue;
          fi
          ;;
        in*)
          if [[ -z "$_not" ]] && [[ "__${_cmp_val}__" != *"__${_cond_val}__"* ]]; then continue;
          elif [[ "$_not" ]] && [[ "__${_cmp_val}__" == *"__${_cond_val}__"* ]]; then continue;
          fi
          ;;
        esac
        ;;
      *)
        log_warn "... unrecognized test operator \\e[1;91m${_test_op}\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
        ;;
      esac
      # matches
      _val=$(eval echo "\$${_target_var}")
      log_info "... apply \\e[32m${_target_var}\\e[0m from \\e[32m\$${_scoped_var}\\e[0m${_val:+ (\\e[33;1moverwrite\\e[0m)}"
      _val=$(eval echo "\$${_scoped_var}")
      export "${_target_var}"="${_val}"
    done
    log_info "... done"
  }

  # evaluate and export a secret
  # - $1: secret variable name
  function eval_secret() {
    name=$1
    value=$(eval echo "\$${name}")
    case "$value" in
    @b64@*)
      decoded=$(mktemp)
      errors=$(mktemp)
      if echo "$value" | cut -c6- | base64 -d > "${decoded}" 2> "${errors}"
      then
        # shellcheck disable=SC2086
        export ${name}="$(cat ${decoded})"
        log_info "Successfully decoded base64 secret \\e[33;1m${name}\\e[0m"
      else
        fail "Failed decoding base64 secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
      fi
      ;;
    @hex@*)
      decoded=$(mktemp)
      errors=$(mktemp)
      if echo "$value" | cut -c6- | sed 's/\([0-9A-F]\{2\}\)/\\\\x\1/gI' | xargs printf > "${decoded}" 2> "${errors}"
      then
        # shellcheck disable=SC2086
        export ${name}="$(cat ${decoded})"
        log_info "Successfully decoded hexadecimal secret \\e[33;1m${name}\\e[0m"
      else
        fail "Failed decoding hexadecimal secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
      fi
      ;;
    @url@*)
      url=$(echo "$value" | cut -c6-)
      if command -v curl > /dev/null
      then
        decoded=$(mktemp)
        errors=$(mktemp)
        if curl -s -S -f --connect-timeout 5 -o "${decoded}" "$url" 2> "${errors}"
        then
          # shellcheck disable=SC2086
          export ${name}="$(cat ${decoded})"
          log_info "Successfully curl'd secret \\e[33;1m${name}\\e[0m"
        else
          log_warn "Failed getting secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
        fi
      elif command -v wget > /dev/null
      then
        decoded=$(mktemp)
        errors=$(mktemp)
        if wget -T 5 -O "${decoded}" "$url" 2> "${errors}"
        then
          # shellcheck disable=SC2086
          export ${name}="$(cat ${decoded})"
          log_info "Successfully wget'd secret \\e[33;1m${name}\\e[0m"
        else
          log_warn "Failed getting secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
        fi
      else
        log_warn "Couldn't get secret \\e[33;1m${name}\\e[0m: no http client found"
      fi
      ;;
    esac
  }

  function eval_secrets() {
    encoded_vars=$(env | grep -Ev '(^|.*_ENV_)scoped__' | awk -F '=' '/^[a-zA-Z0-9_]*=@(b64|hex|url)@/ {print $1}')
    for var in $encoded_vars
    do
      eval_secret "$var"
    done
  }

  function tf_init() {
    opts=$1
    extra_opts=$2

    log_info "--- \\e[32minit\\e[0m"
    log_info "--- Positioned environment (can also be used as TF vars):"
    # shellcheck disable=SC2154
    log_info "--- environment_type: \\e[33;1m${environment_type}\\e[0m"
    # shellcheck disable=SC2154
    log_info "--- environment_name: \\e[33;1m${environment_name}\\e[0m"
    # shellcheck disable=SC2154
    log_info "--- environment_slug: \\e[33;1m${environment_slug}\\e[0m"

    # maybe enable debug log
    if [[ "$TRACE" ]]; then
      export TF_LOG=DEBUG
    fi

    # Use terraform automation mode (will remove some verbose unneeded messages)
    export TF_IN_AUTOMATION=true
    export TF_INPUT=0 # same as '-input=false' option

    # define environment_type, environment_name and environment_slug as TF variables through env rather than on CLI (fails if not declared)
    export TF_VAR_environment_type=$environment_type
    export TF_VAR_environment_name=$environment_name
    export TF_VAR_environment_slug=$environment_slug

    # make output dir
    mkdir -p "$TF_OUTPUT_DIR"

    # dump terraform version
    terraform --version

    # maybe execute pre init script
    prescript="$TF_SCRIPTS_DIR/tf-pre-init.sh"
    if [[ -f "$prescript" ]]; then
      log_info "--- \\e[32mpre-init\\e[0m hook (\\e[33;1m${prescript}\\e[0m) found: execute"
      chmod +x "$prescript"
      "$prescript"
    else
      log_info "--- \\e[32mpre-init\\e[0m hook (\\e[33;1m${prescript}\\e[0m) not found: skip"
    fi

    if [[ "$TF_GITLAB_BACKEND_DISABLED" != "true" ]]
    then
      # impl inspired by GitLab Terraform image script
      # see https://gitlab.com/gitlab-org/terraform-images/-/blob/master/src/bin/gitlab-terraform.sh
      log_info "configuring Terraform to use GitLab as http backend for tfstate  (set \\e[33;1m\$TF_GITLAB_BACKEND_DISABLED\\e[0m to prevent this)"

      # If TF_USERNAME is unset then default to GITLAB_USER_LOGIN
      TF_USERNAME="${TF_USERNAME:-${GITLAB_USER_LOGIN}}"

      # If TF_PASSWORD is unset then default to gitlab-ci-token/CI_JOB_TOKEN
      if [ -z "${TF_PASSWORD}" ]; then
        TF_USERNAME="gitlab-ci-token"
        TF_PASSWORD="${CI_JOB_TOKEN}"
      fi

      # If TF_ADDRESS is unset then default to GitLab backend in current project
      TF_ADDRESS="${TF_ADDRESS:-${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${environment_slug}}"

      # Set variables for the HTTP backend to default to TF_* values
      # see: https://www.terraform.io/docs/language/settings/backends/http.html
      export TF_HTTP_ADDRESS="${TF_HTTP_ADDRESS:-${TF_ADDRESS}}"
      export TF_HTTP_LOCK_ADDRESS="${TF_HTTP_LOCK_ADDRESS:-${TF_ADDRESS}/lock}"
      export TF_HTTP_LOCK_METHOD="${TF_HTTP_LOCK_METHOD:-POST}"
      export TF_HTTP_UNLOCK_ADDRESS="${TF_HTTP_UNLOCK_ADDRESS:-${TF_ADDRESS}/lock}"
      export TF_HTTP_UNLOCK_METHOD="${TF_HTTP_UNLOCK_METHOD:-DELETE}"
      export TF_HTTP_USERNAME="${TF_HTTP_USERNAME:-${TF_USERNAME}}"
      export TF_HTTP_PASSWORD="${TF_HTTP_PASSWORD:-${TF_PASSWORD}}"
      export TF_HTTP_RETRY_WAIT_MIN="${TF_HTTP_RETRY_WAIT_MIN:-5}"

      # terraform < 0.13.2 doesn't support env var..
      tf_backend_opts="-backend-config=address=${TF_HTTP_ADDRESS}"
      tf_backend_opts="$tf_backend_opts -backend-config=lock_address=${TF_HTTP_LOCK_ADDRESS}"
      tf_backend_opts="$tf_backend_opts -backend-config=unlock_address=${TF_HTTP_UNLOCK_ADDRESS}"
      tf_backend_opts="$tf_backend_opts -backend-config=username=${TF_HTTP_USERNAME}"
      tf_backend_opts="$tf_backend_opts -backend-config=password=${TF_HTTP_PASSWORD}"
      tf_backend_opts="$tf_backend_opts -backend-config=lock_method=${TF_HTTP_LOCK_METHOD}"
      tf_backend_opts="$tf_backend_opts -backend-config=unlock_method=${TF_HTTP_UNLOCK_METHOD}"
      tf_backend_opts="$tf_backend_opts -backend-config=retry_wait_min=${TF_HTTP_RETRY_WAIT_MIN}"
    else
      backend_cfg=$(ls -1 "${environment_type}.tfbackend" 2>/dev/null || ls -1 "default.tfbackend" 2>/dev/null || echo "")
      if [[ -f "$backend_cfg" ]]
      then
        log_info "--- backend config file (\\e[33;1m${backend_cfg}\\e[0m) found: use"
        tf_backend_opts="-backend-config=${backend_cfg}"
      else
        log_info "--- no backend config file found: ignore"
      fi
    fi

    # shellcheck disable=SC2154,SC2086,SC2046
    terraform init -reconfigure $tf_backend_opts $(echo "$extra_opts" | envsubst_cli) $(echo "$opts" | envsubst_cli)
  }

  tf_plan() {
    opts=$1
    extra_opts=$2
    tf_plan=$3
    gitlab_report=$4

    # shellcheck disable=SC2154
    log_info "--- \\e[32mplan\\e[0m"

    # implicit tfvars
    env_vars=$(ls -1 "${environment_type}.env.tfvars" 2>/dev/null || ls -1 "${environment_type}.env.tfvars.json" 2>/dev/null || echo "")
    if [[ -f "$env_vars" ]]
    then
      log_info "--- environment-specific tfvars file (\\e[33;1m${env_vars}\\e[0m) found: use"
    else
      log_info "--- no environment-specific tfvars file found: ignore"
    fi

    # shellcheck disable=SC2154,SC2086,SC2046
    terraform plan ${env_vars:+-var-file=${env_vars}} -out "$tf_plan" $(echo "$extra_opts" | envsubst_cli) $(echo "$opts" | envsubst_cli)

    # then generate GitLab TF report
    if ! command -v jq > /dev/null
    then
      log_info "--- installing jq (required to generate GitLab report)..."
      apk add --no-cache jq
    fi
    log_info "--- now generating GitLab report..."
    # impl inspired by GitLab Terraform template
    # see https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/Terraform.gitlab-ci.yml
    terraform show --json "$tf_plan" | jq -r '([.resource_changes[]?.change.actions?]|flatten)|{"create":(map(select(.=="create"))|length),"update":(map(select(.=="update"))|length),"delete":(map(select(.=="delete"))|length)}' > "$gitlab_report"
  }

  function tf_apply() {
    opts=$1
    extra_opts=$2
    # optional arg
    tf_plan=$3

    # shellcheck disable=SC2154
    log_info "--- \\e[32mapply\\e[0m"

    # maybe execute pre apply script
    prescript="$TF_SCRIPTS_DIR/tf-pre-apply.sh"
    if [[ -f "$prescript" ]]; then
      log_info "--- \\e[32mpre-apply\\e[0m hook (\\e[33;1m${prescript}\\e[0m) found: execute"
      chmod +x "$prescript"
      "$prescript"
    else
      log_info "--- \\e[32mpre-apply\\e[0m hook (\\e[33;1m${prescript}\\e[0m) not found: skip"
    fi

    if [[ "$tf_plan" ]]; then
      log_info "--- applying upstream plan: \\e[33;1m${tf_plan}\\e[0m"
      terraform apply -auto-approve "$tf_plan"
    else
      # implicit tfvars
      env_vars=$(ls -1 "${environment_type}.env.tfvars" 2>/dev/null || ls -1 "${environment_type}.env.tfvars.json" 2>/dev/null || echo "")
      if [[ -f "$env_vars" ]]
      then
        log_info "--- environment-specific tfvars file (\\e[33;1m${env_vars}\\e[0m) found: use"
      else
        log_info "--- no environment-specific tfvars file found: ignore"
      fi
      log_info "--- auto plan + apply"
      # shellcheck disable=SC2154,SC2086,SC2046
      terraform apply -auto-approve ${env_vars:+-var-file=${env_vars}} $(echo "$extra_opts" | envsubst_cli) $(echo "$opts" | envsubst_cli)
    fi

    # maybe execute post apply script
    postscript="$TF_SCRIPTS_DIR/tf-post-apply.sh"
    if [[ -f "$postscript" ]]; then
      log_info "--- \\e[32mpost-apply\\e[0m hook (\\e[33;1m${postscript}\\e[0m) found: execute"
      chmod +x "$postscript"
      "$postscript"
    else
      log_info "--- \\e[32mpost-apply\\e[0m hook (\\e[33;1m${postscript}\\e[0m) not found: skip"
    fi

    # finally propagate environment info
    echo -e "environment_type=$environment_type\\nenvironment_name=$CI_ENVIRONMENT_NAME\\nenvironment_slug=$CI_ENVIRONMENT_SLUG" >> terraform.env
  }

  function tf_destroy() {
    opts=$1
    extra_opts=$2

    # shellcheck disable=SC2154
    log_info "--- \\e[32mdestroy\\e[0m"

    # maybe execute pre destroy script
    prescript="$TF_SCRIPTS_DIR/tf-pre-destroy.sh"
    if [[ -f "$prescript" ]]; then
      log_info "--- \\e[32mpre-destroy\\e[0m hook (\\e[33;1m${prescript}\\e[0m) found: execute"
      chmod +x "$prescript"
      "$prescript"
    else
      log_info "--- \\e[32mpre-destroy\\e[0m hook (\\e[33;1m${prescript}\\e[0m) not found: skip"
    fi

    # implicit tfvars
    env_vars=$(ls -1 "${environment_type}.env.tfvars" 2>/dev/null || ls -1 "${environment_type}.env.tfvars.json" 2>/dev/null || echo "")
    if [[ -f "$env_vars" ]]
    then
      log_info "--- environment-specific tfvars file (\\e[33;1m${env_vars}\\e[0m) found: use"
    else
      log_info "--- no environment-specific tfvars file found: ignore"
    fi

    # shellcheck disable=SC2154,SC2086,SC2046
    terraform destroy -auto-approve ${env_vars:+-var-file=${env_vars}} $(echo "$extra_opts" | envsubst_cli) $(echo "$opts" | envsubst_cli)

    # remove gitlab-managed tf state
    if [[ "$TF_GITLAB_BACKEND_DISABLED" != "true" ]]
    then
      if ! command -v curl > /dev/null
      then
        log_info "--- installing curl (required to remove GitLab terraform state)..."
        apk add --no-cache curl
      fi
      curl --header "Private-Token: ${TF_PASSWORD}" --request DELETE "${TF_ADDRESS}"
    fi

    # maybe execute post destroy script
    postscript="$TF_SCRIPTS_DIR/tf-post-destroy.sh"
    if [[ -f "$postscript" ]]; then
      log_info "--- \\e[32mpost-destroy\\e[0m hook (\\e[33;1m${postscript}\\e[0m) found: execute"
      chmod +x "$postscript"
      "$postscript"
    else
      log_info "--- \\e[32mpost-destroy\\e[0m hook (\\e[33;1m${postscript}\\e[0m) not found: skip"
    fi
  }

  tf_infracost() {
    # shellcheck disable=SC2154
    log_info "--- \\e[32minfracost\\e[0m"

    if [[ "${TF_INFACOST_USAGE_FILE}" ]];then
      tf_usagefile="--sync-usage-file --usage-file ${TF_INFACOST_USAGE_FILE}"
    fi
    # shellcheck disable=SC2086
    infracost ${TF_INFRACOST_ARGS} ${tf_usagefile} --path .
  }

  unscope_variables
  eval_secrets

  # ENDSCRIPT

.tf-commands:
  default:
    init: tf_init "${ENV_INIT_OPTS:-$TF_INIT_OPTS}" "${ENV_EXTRA_OPTS:-$TF_EXTRA_OPTS}"
    plan: tf_plan "${ENV_PLAN_OPTS:-$TF_PLAN_OPTS}" "${ENV_EXTRA_OPTS:-$TF_EXTRA_OPTS}" "${ENV_TYPE}.tfplan" "${ENV_TYPE}-plan.json"
    apply: tf_apply "${ENV_APPLY_OPTS:-$TF_APPLY_OPTS}" "${ENV_EXTRA_OPTS:-$TF_EXTRA_OPTS}" "$tfplan"
    destroy: tf_destroy "${ENV_DESTROY_OPTS:-$TF_DESTROY_OPTS}" "${ENV_EXTRA_OPTS:-$TF_EXTRA_OPTS}"
  init:
    - !reference [ .tf-commands, default, init ]
  plan:
    - !reference [ .tf-commands, default, plan ]
  apply:
    - !reference [ .tf-commands, default, apply ]
  destroy:
    - !reference [ .tf-commands, default, destroy ]

# =============================================================================
# === Job Prototypes
# =============================================================================
# job prototype
# defines default Docker image, tracking probe, cache policy
# @arg ENV_TYPE      : environment type
# @arg ENV_INIT_OPTS : environment specific tf init options
# @arg ENV_EXTRA_OPTS: environment specific tf extra options (all commands)
.tf-base:
  image:
    name: "$TF_IMAGE"
    entrypoint: [""]
  services:
    - name: "$TBC_TRACKING_IMAGE"
      command: ["--service", "terraform", "3.4.0" ]
  before_script:
    # set environment context (before unscope_variables)
    - export environment_type=$ENV_TYPE
    - export environment_name=$CI_ENVIRONMENT_NAME
    - export environment_slug=$CI_ENVIRONMENT_SLUG
    - *tf-scripts
    - install_ca_certs "${CUSTOM_CA_CERTS:-$DEFAULT_CA_CERTS}"
    - cd "$TF_PROJECT_DIR"
    - !reference [ .tf-commands, init ]
  cache:
    key: "$CI_COMMIT_REF_SLUG-terraform"
    paths:
      - $TF_PROJECT_DIR/.terraform/
      - $TF_PROJECT_DIR/.terraform.lock.hcl

# Create job prototype
# @arg ENV_PLAN_ENABLED : environment specific plan to apply (if transfered from upstream jobs)
# @arg ENV_APPLY_OPTS   : environment specific tf apply options
.tf-create:
  extends: .tf-base
  stage: infra
  script:
    - if [[ "$ENV_PLAN_ENABLED" == "true" ]]; then tfplan="${ENV_TYPE}.tfplan"; fi
    - !reference [ .tf-commands, apply ]
  artifacts:
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    paths:
      - $TF_PROJECT_DIR/$TF_OUTPUT_DIR/
    reports:
      dotenv: $TF_PROJECT_DIR/terraform.env

# plan job for production (on build)
# @arg ENV_PLAN_OPTS : environment specific tf plan options
.tf-plan:
  extends: .tf-base
  stage: build
  script:
    - !reference [ .tf-commands, plan ]
  artifacts:
    name: "Terraform plan for $ENV_TYPE from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    paths:
      - $TF_PROJECT_DIR/${ENV_TYPE}.tfplan
    reports:
      terraform: $TF_PROJECT_DIR/${ENV_TYPE}-plan.json

# Destroy job prototype
# @arg ENV_DESTROY_OPTS: environment tf destroy arguments
.tf-destroy:
  extends: .tf-base
  stage: infra
  # force no dependencies
  dependencies: []
  script:
    - !reference [ .tf-commands, destroy ]

tf-tfsec:
  extends: .tf-base
  image:
    name : $TF_TFSEC_IMAGE
    entrypoint: [""]
  stage: test
  needs: []
  before_script:
    - *tf-scripts
    - cd "$TF_PROJECT_DIR"
    - mkdir -p -m 777 reports
  script:
    # tfsec allows generating several report formats at once using --format lovely,junit
    # but the --out option defines the report base name, and each report is generated as "<basename>.format"
    - tfsec --soft-fail --format junit --out "reports/tfsec.xunit.xml" $TF_TFSEC_ARGS
    - |
      if [[ "$DEFECTDOJO_TFSEC_REPORTS" ]]
      then
        tfsec --soft-fail --format json --out "reports/tfsec.native.json" $TF_TFSEC_ARGS
      fi
    - tfsec ${TRACE+--verbose} $TF_TFSEC_ARGS
  artifacts:
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    reports:
      junit: $TF_PROJECT_DIR/reports/tfsec.xunit.xml
    paths:
      - "$TF_PROJECT_DIR/reports/tfsec.*"
  dependencies: []
  rules:
    - if: '$TF_TFSEC_ENABLED != "true"'
      when: never
    - !reference [.test-policy, rules]

tf-tflint:
  extends: .tf-base
  image:
    name: $TF_TFLINT_IMAGE
  stage: build
  needs: []
  before_script:
    - *tf-scripts
    - install_ca_certs "${CUSTOM_CA_CERTS:-$DEFAULT_CA_CERTS}"
    - cd "$TF_PROJECT_DIR"
    - mkdir -p -m 777 reports
  script:
    - tflint --force --format=junit $TF_TFLINT_ARGS > reports/tflint.xunit.xml
    - tflint ${TRACE+--loglevel=debug} $TF_TFLINT_ARGS
  artifacts:
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    reports:
      junit: $TF_PROJECT_DIR/reports/tflint.xunit.xml
    paths:
      - "$TF_PROJECT_DIR/reports/tflint.*"
  dependencies: []
  rules:
    - if: '$TF_TFLINT_DISABLED == "true"'
      when: never
    - !reference [.test-policy, rules]

tf-checkov:
  extends: .tf-base
  image:
    name: $TF_CHECKOV_IMAGE
  stage: test
  needs: []
  before_script:
    - *tf-scripts
    - install_ca_certs "${CUSTOM_CA_CERTS:-$DEFAULT_CA_CERTS}"
    - cd "$TF_PROJECT_DIR"
    - mkdir -p -m 777 reports
  script:
    # checkov allows generating several report formats at once using multiple --output options
    # and --output-file-path but the option defines an output directory, and report filenames
    # can't be chosen ("results_junitxml.xml" and "results_cli.txt")
    - checkov --soft-fail --output junitxml $TF_CHECKOV_ARGS > "reports/checkov.xunit.xml"
    - |
      if [[ "$DEFECTDOJO_CHECKOV_REPORTS" ]]
      then
        checkov --soft-fail --output json $TF_CHECKOV_ARGS > "reports/checkov.native.json"
      fi
    - checkov $TF_CHECKOV_ARGS
  artifacts:
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    reports:
      junit: $TF_PROJECT_DIR/reports/checkov.xunit.xml
    paths:
      - "$TF_PROJECT_DIR/reports/checkov.*"
  dependencies: []
  rules:
    - if: '$TF_CHECKOV_ENABLED != "true"'
      when: never
    - !reference [.test-policy, rules]

tf-infracost:
  extends: .tf-base
  image:
    name : $TF_INFRACOST_IMAGE
    entrypoint: [""]
  stage: test
  needs: []
  before_script:
    - *tf-scripts
    - cd "$TF_PROJECT_DIR"
  script:
    - tf_infracost
  rules:
    - if: '$TF_INFRACOST_ENABLED != "true"'
      when: never
    - !reference [.test-policy, rules]

tf-fmt:
  extends: .tf-base
  stage: test
  needs: []
  before_script:
    - *tf-scripts
    - cd "$TF_PROJECT_DIR"
  script:
    - terraform fmt -check $TF_FMT_ARGS
  dependencies: []
  rules:
    - if: '$TF_FMT_ENABLED != "true"'
      when: never
    - !reference [.test-policy, rules]

# =============================================================================
# === Review env jobs
# =============================================================================
# plan job for review (on build)
tf-plan-review:
  extends: .tf-plan
  variables:
    ENV_TYPE: review
    ENV_EXTRA_OPTS: $TF_REVIEW_EXTRA_OPTS
    ENV_INIT_OPTS: $TF_REVIEW_INIT_OPTS
    ENV_PLAN_OPTS: $TF_REVIEW_PLAN_OPTS
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: prepare
  resource_group: tf-review/$CI_COMMIT_REF_NAME
  rules:
    # exclude tags
    - if: $CI_COMMIT_TAG
      when: never
    # exclude production & integration branches
    - if: '$CI_COMMIT_REF_NAME =~ $PROD_REF || $CI_COMMIT_REF_NAME =~ $INTEG_REF'
      when: never
    # exclude if $TF_REVIEW_PLAN_ENABLED unset
    - if: '$TF_REVIEW_PLAN_ENABLED != "true"'
      when: never
    # if $TF_REVIEW_ENABLED: auto
    - if: '$TF_REVIEW_ENABLED == "true"'

# create review env (only on feature branches)
tf-review:
  extends: .tf-create
  variables:
    ENV_TYPE: review
    ENV_EXTRA_OPTS: $TF_REVIEW_EXTRA_OPTS
    ENV_INIT_OPTS: $TF_REVIEW_INIT_OPTS
    ENV_APPLY_OPTS: $TF_REVIEW_APPLY_OPTS
    ENV_PLAN_ENABLED: $TF_REVIEW_PLAN_ENABLED
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: start
  resource_group: tf-review/$CI_COMMIT_REF_NAME
  rules:
    # exclude tags
    - if: $CI_COMMIT_TAG
      when: never
    # exclude production & integration branches
    - if: '$CI_COMMIT_REF_NAME =~ $PROD_REF || $CI_COMMIT_REF_NAME =~ $INTEG_REF'
      when: never
    # exclude if $TF_REVIEW_ENABLED not set
    - if: '$TF_REVIEW_ENABLED != "true"'
      when: never
    # if $TF_REVIEW_PLAN_ENABLED unset: auto
    - if: '$TF_REVIEW_PLAN_ENABLED != "true"'
    # else: manual, blocking
    - if: $CI_COMMIT_REF_NAME # useless test, just to prevent GitLab warning
      when: manual

# destroy review env
tf-destroy-review:
  extends: .tf-destroy
  variables:
    ENV_TYPE: review
    ENV_EXTRA_OPTS: $TF_REVIEW_EXTRA_OPTS
    ENV_INIT_OPTS: $TF_REVIEW_INIT_OPTS
    ENV_DESTROY_OPTS: $TF_REVIEW_DESTROY_OPTS
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  resource_group: tf-review/$CI_COMMIT_REF_NAME
  rules:
    # exclude tags
    - if: $CI_COMMIT_TAG
      when: never
    # only on non-production, non-integration branches, with $TF_REVIEW_ENABLED set
    - if: '$TF_REVIEW_ENABLED == "true" && $CI_COMMIT_REF_NAME !~ $PROD_REF && $CI_COMMIT_REF_NAME !~ $INTEG_REF'
      when: manual
      allow_failure: true

# =============================================================================
# === Integration env jobs
# =============================================================================
# plan job for integration
tf-plan-integration:
  extends: .tf-plan
  variables:
    ENV_TYPE: integration
    ENV_EXTRA_OPTS: $TF_INTEG_EXTRA_OPTS
    ENV_INIT_OPTS: $TF_INTEG_INIT_OPTS
    ENV_PLAN_OPTS: $TF_INTEG_PLAN_OPTS
  environment:
    name: integration
    action: prepare
  resource_group: tf-integration
  rules:
    # exclude non-integration branches
    - if: '$CI_COMMIT_REF_NAME !~ $INTEG_REF'
      when: never
    # exclude if $TF_INTEG_PLAN_ENABLED unset
    - if: '$TF_INTEG_PLAN_ENABLED != "true"'
      when: never
    # if $TF_INTEG_ENABLED: auto
    - if: '$TF_INTEG_ENABLED == "true"'

# create integration env (only on develop branch)
tf-integration:
  extends: .tf-create
  variables:
    ENV_TYPE: integration
    ENV_EXTRA_OPTS: $TF_INTEG_EXTRA_OPTS
    ENV_INIT_OPTS: $TF_INTEG_INIT_OPTS
    ENV_APPLY_OPTS: $TF_INTEG_APPLY_OPTS
    ENV_PLAN_ENABLED: $TF_INTEG_PLAN_ENABLED
  environment:
    name: integration
    action: start
  resource_group: tf-integration
  rules:
    # exclude non-integration branches
    - if: '$CI_COMMIT_REF_NAME !~ $INTEG_REF'
      when: never
    # exclude if $TF_INTEG_ENABLED not set
    - if: '$TF_INTEG_ENABLED != "true"'
      when: never
    # if $TF_INTEG_PLAN_ENABLED unset: auto
    - if: '$TF_INTEG_PLAN_ENABLED != "true"'
    # else: manual, blocking
    - if: $CI_COMMIT_REF_NAME # useless test, just to prevent GitLab warning
      when: manual

# destroy integration env
tf-destroy-integration:
  extends: .tf-destroy
  variables:
    ENV_TYPE: integration
    ENV_EXTRA_OPTS: $TF_INTEG_EXTRA_OPTS
    ENV_INIT_OPTS: $TF_INTEG_INIT_OPTS
    ENV_DESTROY_OPTS: $TF_INTEG_DESTROY_OPTS
  environment:
    name: integration
    action: stop
  resource_group: tf-integration
  rules:
    # only on integration branch(es), with $TF_INTEG_ENABLED set
    - if: '$TF_INTEG_ENABLED == "true" && $CI_COMMIT_REF_NAME =~ $INTEG_REF'
      when: manual
      allow_failure: true

# =============================================================================
# === Staging env jobs
# =============================================================================
# plan job for staging
tf-plan-staging:
  extends: .tf-plan
  variables:
    ENV_TYPE: staging
    ENV_EXTRA_OPTS: $TF_STAGING_EXTRA_OPTS
    ENV_INIT_OPTS: $TF_STAGING_INIT_OPTS
    ENV_PLAN_OPTS: $TF_STAGING_PLAN_OPTS
  environment:
    name: staging
    action: prepare
  resource_group: tf-staging
  rules:
    # exclude non-production branches
    - if: '$CI_COMMIT_REF_NAME !~ $PROD_REF'
      when: never
    # exclude if $TF_STAGING_PLAN_ENABLED unset
    - if: '$TF_STAGING_PLAN_ENABLED != "true"'
      when: never
    # if $TF_STAGING_ENABLED: auto
    - if: '$TF_STAGING_ENABLED == "true"'

# create staging env (only on master branch)
tf-staging:
  extends: .tf-create
  variables:
    ENV_TYPE: staging
    ENV_EXTRA_OPTS: $TF_STAGING_EXTRA_OPTS
    ENV_INIT_OPTS: $TF_STAGING_INIT_OPTS
    ENV_APPLY_OPTS: $TF_STAGING_APPLY_OPTS
    ENV_PLAN_ENABLED: $TF_STAGING_PLAN_ENABLED
  environment:
    name: staging
    action: start
  resource_group: tf-staging
  rules:
    # exclude non-production branches
    - if: '$CI_COMMIT_REF_NAME !~ $PROD_REF'
      when: never
    # exclude if $TF_STAGING_ENABLED not set
    - if: '$TF_STAGING_ENABLED != "true"'
      when: never
    # if $TF_STAGING_PLAN_ENABLED unset: auto
    - if: '$TF_STAGING_PLAN_ENABLED != "true"'
    # else: manual, blocking
    - if: $CI_COMMIT_REF_NAME # useless test, just to prevent GitLab warning
      when: manual

# destroy staging env
tf-destroy-staging:
  extends: .tf-destroy
  variables:
    ENV_TYPE: staging
    ENV_EXTRA_OPTS: $TF_STAGING_EXTRA_OPTS
    ENV_INIT_OPTS: $TF_STAGING_INIT_OPTS
    ENV_DESTROY_OPTS: $TF_STAGING_DESTROY_OPTS
  environment:
    name: staging
    action: stop
  resource_group: tf-staging
  rules:
    # only on production branch(es), with $TF_STAGING_ENABLED set
    - if: '$TF_STAGING_ENABLED == "true" && $CI_COMMIT_REF_NAME =~ $PROD_REF'
      when: manual
      allow_failure: true

# =============================================================================
# === Production env jobs
# =============================================================================
# plan job for production: on ALL BRANCHES (on master: pre-computes the plan; on others: computes the diff)
tf-plan-production:
  extends: .tf-plan
  variables:
    ENV_TYPE: production
    ENV_EXTRA_OPTS: $TF_PROD_EXTRA_OPTS
    ENV_INIT_OPTS: $TF_PROD_INIT_OPTS
    ENV_PLAN_OPTS: $TF_PROD_PLAN_OPTS
  environment:
    name: production
    action: prepare
  resource_group: tf-production
  rules:
    # exclude tags
    - if: $CI_COMMIT_TAG
      when: never
    # exclude if $TF_PROD_ENABLED not set
    - if: '$TF_PROD_ENABLED != "true"'
      when: never
    # exclude if $TF_PROD_PLAN_ENABLED not set
    - if: '$TF_PROD_PLAN_ENABLED != "true"'
      when: never
    # enabled on merge requests (2 rules depending on the selected workflow)
    - if: $CI_OPEN_MERGE_REQUESTS
    - if: '$CI_MERGE_REQUEST_ID && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ $PROD_REF'
    # enabled on production branches
    - if: '$CI_COMMIT_REF_NAME =~ $PROD_REF'

# create production env if on branch master and variable TF_PROD_ENABLED defined and AUTODEPLOY_TO_PROD is set
tf-production:
  extends: .tf-create
  stage: infra-prod
  variables:
    ENV_TYPE: production
    ENV_EXTRA_OPTS: $TF_PROD_EXTRA_OPTS
    ENV_INIT_OPTS: $TF_PROD_INIT_OPTS
    ENV_APPLY_OPTS: $TF_PROD_APPLY_OPTS
    ENV_PLAN_ENABLED: $TF_PROD_PLAN_ENABLED
  environment:
    name: production
    action: start
  resource_group: tf-production
  rules:
    # exclude non-production branches
    - if: '$CI_COMMIT_REF_NAME !~ $PROD_REF'
      when: never
    # exclude if $TF_PROD_ENABLED not set
    - if: '$TF_PROD_ENABLED != "true"'
      when: never
    # if $TF_PROD_PLAN_ENABLED unset: auto
    - if: '$TF_PROD_PLAN_ENABLED != "true"'
    # else: manual, blocking
    - if: $CI_COMMIT_REF_NAME # useless test, just to prevent GitLab warning
      when: manual
