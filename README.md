# GitLab CI template for Terraform

This project implements a generic GitLab CI template for managing infrastructure with [Terraform](https://www.terraform.io/).

## Overview

This template implements continuous delivery/continuous deployment for projects hosted on Terraform platforms.

It provides several features, usable in different modes.

### Review environments

The template supports **review** environments: those are dynamic and ephemeral environments to deploy your
_ongoing developments_ (a.k.a. _feature_ or _topic_ branches).

When enabled, it deploys the result from upstream build stages to a dedicated and temporary environment.
It is only active for non-production, non-integration branches.

It is a strict equivalent of GitLab's [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/) feature.

It also comes with a _cleanup_ job (accessible either from the _environments_ page, or from the pipeline view).

### Integration environment

If you're using a Git Workflow with an integration branch (such as [Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)),
the template supports an **integration** environment.

When enabled, it deploys the result from upstream build stages to a dedicated environment.
It is only active for your integration branch (`develop` by default).

### Production environments

Lastly, the template supports 2 environments associated to your production branch (`master` by default):

* a **staging** environment (an iso-prod environment meant for testing and validation purpose),
* the **production** environment.

You're free to enable whichever or both, and you can also choose your deployment-to-production policy:

* **continuous deployment**: automatic deployment to production (when the upstream pipeline is successful),
* **continuous delivery**: deployment to production can be triggered manually (when the upstream pipeline is successful).

## Usage

### Include

In order to include this template in your project, add the following to your `gitlab-ci.yml`:

```yaml
include:
  - project: 'to-be-continuous/terraform'
    ref: '3.4.0'
    file: '/templates/gitlab-ci-terraform.yml'
```

### Template jobs

The Terraform template implements - for each environment presented above - the following jobs:

* environment creation [plan](https://www.terraform.io/docs/cli/commands/plan.html): this job is **optional** and computes the environment changes before applying them. When enabled, the env creation has to be applied manually. By default, this job is enabled only for the **production** environment.
* environment creation [apply](https://www.terraform.io/docs/cli/commands/apply.html): applies the environment changes, whether manually by applying the upstream plan (when enabled), or automatically.
* environment [destruction](https://www.terraform.io/docs/cli/commands/destroy.html): can be executed manually on non-production envs only.

### Global configuration

The Terraform template uses some global configuration used throughout all jobs.

| Name                     | description                            | default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `TF_IMAGE`               | the Docker image used to run Terraform CLI commands <br/>:warning: **set the version required by your project** | `hashicorp/terraform:light` |
| `TF_GITLAB_BACKEND_DISABLED`| Set to `true` to disable [GitLab managed Terraform State](https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html) | _none_ (enabled) |
| `TF_PROJECT_DIR`         | Terraform project root directory        | `.`               |
| `TF_SCRIPTS_DIR`         | Terraform (hook) scripts base directory (relative to `$TF_PROJECT_DIR`) | `.` |
| `TF_OUTPUT_DIR`          | Terraform output directory (relative to `$TF_PROJECT_DIR`). Everything generated in this directory will be kept as job artifacts. | `tf-output` |
| `TF_EXTRA_OPTS`          | Default Terraform extra options (applies to all Terraform commands) | _none_ |
| `TF_INIT_OPTS`           | Default Terraform extra [init options](https://www.terraform.io/docs/cli/commands/init.html) | _none_ |
| `TF_APPLY_OPTS`          | Default Terraform extra [apply options](https://www.terraform.io/docs/cli/commands/apply.html) | _none_ |
| `TF_DESTROY_OPTS`        | Default Terraform extra [destroy options](https://www.terraform.io/docs/cli/commands/destroy.html) | _none_ |

### Secrets management

Here are some advices about your **secrets** (variables marked with a :lock:):

1. Manage them as [project or group CI/CD variables](https://docs.gitlab.com/ee/ci/variables/#create-a-custom-variable-in-the-ui):
    * [**masked**](https://docs.gitlab.com/ee/ci/variables/#mask-a-custom-variable) to prevent them from being inadvertently
      displayed in your job logs,
    * [**protected**](https://docs.gitlab.com/ee/ci/variables/#protect-a-custom-variable) if you want to secure some secrets
      you don't want everyone in the project to have access to (for instance production secrets).
2. In case a secret contains [characters that prevent it from being masked](https://docs.gitlab.com/ee/ci/variables/#masked-variable-requirements),
  simply define its value as the [Base64](https://en.wikipedia.org/wiki/Base64) encoded value prefixed with `@b64@`:
  it will then be possible to mask it and the template will automatically decode it prior to using it.
3. Don't forget to escape special characters (ex: `$` -> `$$`).

### Terraform integration in Merge Requests

This template enables [Terraform integration in Merge Requests](https://docs.gitlab.com/ee/user/infrastructure/mr_integration.html).

As a result if you enabled your `production` environment, every merge request will compute and display infrastructure changes compared to `master` branch.

### Terraform Backend management

By default, this template enables [GitLab managed Terraform State](https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html).
As mentionned in GitLab's documentation, that requires that your Terraform scripts declare the
Terraform [HTTP backend](https://www.terraform.io/docs/language/settings/backends/http.html), the templates
does the rest to configure it automatically.

This default behavior can be disabled by setting `$TF_GITLAB_BACKEND_DISABLED` to `true`.
In that case, you'll have to declare and configure your backend and tfstate by yourself (see [Implicit Backend configuration support](#implicit-backend-configuration-support) below).

#### _Error acquiring the state lock_ workaround

The template takes care of [configuring the http backend](https://www.terraform.io/docs/language/settings/backends/http.html#configuration-variables),
including with authentication credentials (using GitLab job token).

Anyway - depending on the Terraform version you are using - you _may_ face this error when applying a plan that was computed in an upstream job:

> Error locking state: Error acquiring the state lock: HTTP remote state endpoint requires auth

This is [a known issue](https://gitlab.com/gitlab-org/terraform-images/-/issues/9). A simple workaround is to create a [Project Access Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) with API rights, then declare it as a masked secret variable with name `TF_PASSWORD` in your Terraform project.

#### How to use GitLab backend in your development environment ?

First create a [Project Access Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) or [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#personal-access-tokens).

In your shell terminal, execute the following script:

```bash
#!/bin/bash

# TODO: replace 3 next variables
MY_PROJECT_PATH="path/to/my-project"
MY_ENV_NAME="dev"
TF_HTTP_PASSWORD="YOUR-ACCESS-TOKEN"

CI_API_V4_URL=https://gitlab.com/api/v4
CI_PROJECT_ID=${MY_PROJECT_PATH//\//%2f}

TF_HTTP_ADDRESS="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/$MY_ENV_NAME"
TF_HTTP_LOCK_ADDRESS="${TF_HTTP_ADDRESS}/lock"
TF_HTTP_LOCK_METHOD="POST"
TF_HTTP_UNLOCK_ADDRESS="${TF_HTTP_ADDRESS}/lock"
TF_HTTP_UNLOCK_METHOD="DELETE"
TF_HTTP_USERNAME="gitlab-token"
TF_HTTP_RETRY_WAIT_MIN="5"

terraform -v

terraform init \
    -reconfigure \
    -backend-config=address="${TF_HTTP_ADDRESS}" \
    -backend-config=lock_address="${TF_HTTP_LOCK_ADDRESS}" \
    -backend-config=unlock_address="${TF_HTTP_UNLOCK_ADDRESS}" \
    -backend-config=username="${TF_HTTP_USERNAME}" \
    -backend-config=password="${TF_HTTP_PASSWORD}" \
    -backend-config=lock_method="${TF_HTTP_LOCK_METHOD}" \
    -backend-config=unlock_method="${TF_HTTP_UNLOCK_METHOD}" \
    -backend-config=retry_wait_min="${TF_HTTP_RETRY_WAIT_MIN}"
```

#### Implicit Backend configuration support

If you disabled the GitLab-managed Terraform state (by setting `$TF_GITLAB_BACKEND_DISABLED` to `true`),
the template supports an implicit [backend configuration](https://www.terraform.io/language/settings/backends/configuration#file) mechanism:

1. Looks for a `$env.tfbackend` file (ex: `staging.tfbackend` for staging environment),
2. Fallbacks to `default.tfbackend` file.

If one of those files are found, it is automatically used by the template in the `terraform init` command (using the `-backend-config` CLI option).

### Environments configuration

As seen above, the Terraform template may support up to 4 environments (`review`, `integration`, `staging` and `production`).

Here are configuration details for each environment.

#### Review environments

Review environments are dynamic and ephemeral environments to deploy your _ongoing developments_ (a.k.a. _feature_ or _topic_ branches).

They are **disabled by default** and can be enabled by setting the `TF_REVIEW_ENABLED` variable (see below).

Here are variables supported to configure review environments:

| Name                     | description                            | default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `TF_REVIEW_ENABLED`      | Set to `true` to enable your `review` environments | _none_ (disabled) |
| `TF_REVIEW_EXTRA_OPTS`   | Terraform extra options for `review` env (applies to all Terraform commands) | `$TF_EXTRA_OPTS` |
| `TF_REVIEW_INIT_OPTS`    | Terraform extra [init options](https://www.terraform.io/docs/cli/commands/init.html) for `review` env | `$TF_INIT_OPTS` |
| `TF_REVIEW_PLAN_ENABLED` | Set to `true` to enable separate Terraform plan job for `review` env. | _none_ (disabled) |
| `TF_REVIEW_PLAN_OPTS`    | Terraform extra [plan options](https://www.terraform.io/docs/cli/commands/plan.html) for `review` env | _none_ |
| `TF_REVIEW_APPLY_OPTS`   | Terraform extra [apply options](https://www.terraform.io/docs/cli/commands/apply.html) for `review` env | `$TF_APPLY_OPTS` |
| `TF_REVIEW_DESTROY_OPTS` | Terraform extra [destroy options](https://www.terraform.io/docs/cli/commands/destroy.html) for `review` env | `$TF_DESTROY_OPTS` |

#### Integration environment

The integration environment is the environment associated to your integration branch (`develop` by default).

It is **disabled by default** and can be enabled by setting the `TF_INTEG_ENABLED` variable (see below).

Here are variables supported to configure the integration environment:

| Name                     | description                            | default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `TF_INTEG_ENABLED`      | Set to `true` to enable your `integration` env | _none_ (disabled) |
| `TF_INTEG_EXTRA_OPTS`   | Terraform extra options for `integration` env (applies to all Terraform commands) | `$TF_EXTRA_OPTS` |
| `TF_INTEG_INIT_OPTS`    | Terraform extra [init options](https://www.terraform.io/docs/cli/commands/init.html) for `integration` env | `$TF_INIT_OPTS` |
| `TF_INTEG_PLAN_ENABLED` | Set to `true` to enable separate Terraform plan job for `integration` env. | _none_ (disabled) |
| `TF_INTEG_PLAN_OPTS`    | Terraform extra [plan options](https://www.terraform.io/docs/cli/commands/plan.html) for `integration` env | _none_ |
| `TF_INTEG_APPLY_OPTS`   | Terraform extra [apply options](https://www.terraform.io/docs/cli/commands/apply.html) for `integration` env | `$TF_APPLY_OPTS` |
| `TF_INTEG_DESTROY_OPTS` | Terraform extra [destroy options](https://www.terraform.io/docs/cli/commands/destroy.html) for `integration` env | `$TF_DESTROY_OPTS` |

#### Staging environment

The staging environment is an iso-prod environment meant for testing and validation purpose associated to your production
branch (`master` by default).

It is **disabled by default** and can be enabled by setting the `TF_STAGING_ENABLED` variable (see below).

Here are variables supported to configure the staging environment:

| Name                     | description                            | default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `TF_STAGING_ENABLED`      | Set to `true` to enable your `staging` env | _none_ (disabled) |
| `TF_STAGING_EXTRA_OPTS`   | Terraform extra options for `staging` env (applies to all Terraform commands) | `$TF_EXTRA_OPTS` |
| `TF_STAGING_INIT_OPTS`    | Terraform extra [init options](https://www.terraform.io/docs/cli/commands/init.html) for `staging` env | `$TF_INIT_OPTS` |
| `TF_STAGING_PLAN_ENABLED` | Set to `true` to enable separate Terraform plan job for `staging` env. | _none_ (disabled) |
| `TF_STAGING_PLAN_OPTS`    | Terraform extra [plan options](https://www.terraform.io/docs/cli/commands/plan.html) for `staging` env | _none_ |
| `TF_STAGING_APPLY_OPTS`   | Terraform extra [apply options](https://www.terraform.io/docs/cli/commands/apply.html) for `staging` env | `$TF_APPLY_OPTS` |
| `TF_STAGING_DESTROY_OPTS` | Terraform extra [destroy options](https://www.terraform.io/docs/cli/commands/destroy.html) for `staging` env | `$TF_DESTROY_OPTS` |

#### Production environment

The production environment is the final deployment environment associated with your production branch (`master` by default).

It is **disabled by default** and can be enabled by setting the `TF_PROD_ENABLED` variable (see below).

Here are variables supported to configure the production environment:

| Name                     | description                            | default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `TF_PROD_ENABLED`      | Set to `true` to enable your `production` env | _none_ (disabled) |
| `TF_PROD_EXTRA_OPTS`   | Terraform extra options for `production` env (applies to all Terraform commands) | `$TF_EXTRA_OPTS` |
| `TF_PROD_INIT_OPTS`    | Terraform extra [init options](https://www.terraform.io/docs/cli/commands/init.html) for `production` env | `$TF_INIT_OPTS` |
| `TF_PROD_PLAN_ENABLED` | Set to `true` to enable separate Terraform plan job for `production` env. | `true` (enabled) |
| `TF_PROD_PLAN_OPTS`    | Terraform extra [plan options](https://www.terraform.io/docs/cli/commands/plan.html) for `production` env | _none_ |
| `TF_PROD_APPLY_OPTS`   | Terraform extra [apply options](https://www.terraform.io/docs/cli/commands/apply.html) for `production` env | `$TF_APPLY_OPTS` |

### Hook scripts

Terraform jobs also support _optional_ **hook scripts** from your project, located in the `$TF_SCRIPTS_DIR` directory (root project dir by default, but may be overridden).

* `tf-pre-init.sh` is executed **before** running `terraform init`
* `tf-pre-apply.sh` is executed **before** running `terraform apply`
* `tf-post-apply.sh` is executed **after** running `terraform apply`
* `tf-pre-destroy.sh` is executed **before** running `terraform destroy`
* `tf-post-destroy.sh` is executed **after** running `terraform destroy`

### Terraform commands overrides

Instead of creating hook scripts, you can also override and/or decorate the Terraform commands
using predefined `.tf-commands` template block, referenced by the [`!reference` directive](https://docs.gitlab.com/ee/ci/yaml/#reference-tags).

By default, the `.tf-commands`, block is composed as below:

```yaml
.tf-commands:
  init:
    - !reference [ .tf-commands, default, init ]
  plan:
    - !reference [ .tf-commands, default, plan ]
  apply:
    - !reference [ .tf-commands, default, apply ]
  destroy:
    - !reference [ .tf-commands, default, destroy ]
```

You can override it for example in the following way:

```yaml
.tf-commands:
  init:
    - source sandbox.env
    - !reference [ .tf-commands, default, init ]
    - echo "I'm executed after the terraform init command"
```

You can use this mechanism to source to the current shell your own environmental variables.

### Environment & Terraform variables support

You have to be aware that your Terraform code has to be able to cope with various environments
(`review`, `integration`, `staging` and `production`), each with different application names, exposed routes, settings, ...

In order to be able to implement some **genericity** in your code, you should use [Terraform variables](https://www.terraform.io/docs/language/values/variables.html) (in your Terraform files), and environment variables (in your hook scripts):

1. Use [`tfvars` files](https://www.terraform.io/language/values/variables#variable-definitions-tfvars-files) for non-secret configuration:
    * default `terraform.tfvars[.json]` and `*.auto.tfvars[.json]` files are obviously supported by Terraform,
    * the template also auto-detects any file named `$env.env.tfvars[.json]` (ex: `staging.env.tfvars` for staging environment) and uses it with all related `terraform` commands.
2. any [predefined GitLab CI variable](https://docs.gitlab.com/ee/ci/variables/#predefined-environment-variables) may be freedly used in your hook scripts or extra options variables (ex: `TF_EXTRA_OPTS: "-var project_name=$CI_PROJECT_NAME"`)
3. you may also use [custom GitLab variables](https://docs.gitlab.com/ee/ci/variables/#custom-cicd-variables) to pass values to your hook script or even directly as Terraform variables [using the right syntax](https://www.terraform.io/docs/cli/config/environment-variables.html#tf_var_name)
    (ex: env variable `$TF_VAR_ssh_private_key_file` will be visible as `ssh_private_key_file` Terraform variable in your code)
4. **dynamic variables** provided by the template:
    * `environment_type`: the environment type (`review`, `integration`, `staging` or `production`)
    * `environment_name` (set as `$CI_ENVIRONMENT_NAME`): the full environment name (ex: `review/fix-prometheus-configuration`, `integration`, `staging` or `production`)
    * `environment_slug` (set as `$CI_ENVIRONMENT_SLUG`): the _slugified_ environment name (ex: `review-fix-promet-r13zmu`, `integration`, `staging` or `production`)

When managing multiple environments, it is a good practice to **prefix your Terraform resource names with `environment_slug` variable**.

Example:

```terraform
resource "aws_instance" "web_server" {
  name = "myproj_${var.environment_slug}_web_server"
  ...
}
```

#### How to manage separate values per environment?

It may happen that you need to use different configuration variables depending on the environment you are deploying.
For instance separate `GOOGLE_CREDENTIALS` if you're using [Google Provider](https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_reference#full-reference).

For this, you shall either use [GitLab scoped variables](https://docs.gitlab.com/ee/ci/environments/index.html#scoping-environments-with-specs) or our [scoped variables syntax](https://to-be-continuous.gitlab.io/doc/usage/#scoped-variables) to limit/override some variables values, using `$CI_ENVIRONMENT_NAME` as the conditional variable.

Example: different [OpenStack provider configuration](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs#configuration-reference) for production

```yaml
variables:
  # global OpenStack provider configuration
  OS_AUTH_URL: "https://openstack-nonprod.api.domain/v3"
  OS_TENANT_ID: "my-nonprod-tenant"
  OS_TENANT_NAME: "my-project-nonprod"
  OS_INSECURE: "true"
  # OS_USERNAME & OS_PASSWORD are defined as secret GitLab CI variables

  # overridden configuration for production
  scoped__OS_AUTH_URL__if__CI_ENVIRONMENT_NAME__equals__production: "https://openstack-prod.api.domain/v3"
  scoped__OS_TENANT_ID__if__CI_ENVIRONMENT_NAME__equals__production: "my-prod-tenant"
  scoped__OS_TENANT_NAME__if__CI_ENVIRONMENT_NAME__equals__production: "my-project-prod"
  scoped__OS_INSECURE__if__CI_ENVIRONMENT_NAME__equals__production: "false"
  # OS_USERNAME & OS_PASSWORD are overridden as secret GitLab CI variables
```

### Supported job artifacts

The Terraform template supports [job artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html) that your Terraform
code may generate and need to propagate to downstream jobs:

* `$TF_OUTPUT_DIR` directory and all contained files are stored as job artifacts. Allows to propagate **files**.
* If present, the `terraform.env` file is stored as a [dotenv artifact](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv). Allows to propagate **environment variables**.

Examples:

* When used in conjuction with Ansible template, your Terraform script may [generate the Ansible inventory file](https://www.linkbynet.com/produce-an-ansible-inventory-with-terraform) into the `$TF_OUTPUT_DIR` directory.
* When dynamically obtaining a floating IP address, your Terraform script may generate the `terraform.env` file to propated it as an environment variables.

### `tflint` job

[tflint](https://github.com/terraform-linters/tflint) is a Terraform Linter and uses the following variables:

| Name                  | description                              | default value     |
| --------------------- | ---------------------------------------- | ----------------- |
| `TF_TFLINT_IMAGE`     | the Docker image used to run tflint      | `ghcr.io/terraform-linters/tflint-bundle:latest`  |
| `TF_TFLINT_DISABLED`  | Set to `true` to disable tflint                    | _none_ (enabled)  |
| `TF_TFLINT_ARGS`      | tflint extra [options and args](https://github.com/terraform-linters/tflint/#usage) | `--enable-plugin=google --enable-plugin=azurerm --enable-plugin=aws` |

In addition to a textual report in the console, this job produces the following reports, kept for one day:

| Report         | Format                                                                       | Usage             |
| -------------- | ---------------------------------------------------------------------------- | ----------------- |
| `$TF_PROJECT_DIR/reports/tflint.xunit.xml` | [xUnit](https://en.wikipedia.org/wiki/XUnit) test report(s) | [GitLab integration](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsjunit) |

### `tfsec` job

[tfsec](https://github.com/tfsec/tfsec) uses static analysis of your terraform templates to spot potential security issues and uses the following variables:

| Name                  | description                              | default value     |
| --------------------- | ---------------------------------------- | ----------------- |
| `TF_TFSEC_IMAGE`      | the Docker image used to run tfsec       | `tfsec/tfsec-ci`  |
| `TF_TFSEC_ENABLED`    | Set to `true` to enable tfsec            | _none_ (disabled) |
| `TF_TFSEC_ARGS`       | tfsec [options and args](https://aquasecurity.github.io/tfsec/latest/guides/usage/) | `.` |

In addition to a textual report in the console, this job produces the following reports, kept for one day:

| Report         | Format                                                                       | Usage             |
| -------------- | ---------------------------------------------------------------------------- | ----------------- |
| `$TF_PROJECT_DIR/reports/tfsec.xunit.xml` | [xUnit](https://en.wikipedia.org/wiki/XUnit) test report(s) | [GitLab integration](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsjunit) |
| `$TF_PROJECT_DIR/reports/tfsec.native.json` | tfsec JSON | [DefectDojo integration](https://defectdojo.github.io/django-DefectDojo/integrations/parsers/#tfsec)<br/>_This report is generated only if DefectDojo template is detected_ |

### `checkov` job

[checkov](https://github.com/bridgecrewio/checkov) is a static code analysis tool for infrastructure-as-code and uses the following variables:

| Name                 | description                                                                                | default value        |
| -------------------- | ------------------------------------------------------------------------------------------ | -------------------- |
| `TF_CHECKOV_IMAGE`   | the Docker image used to run checkov                                                       | `bridgecrew/checkov` |
| `TF_CHECKOV_ENABLED` | Set to `true` to enable checkov                                                            | _none_ (disabled)    |
| `TF_CHECKOV_ARGS`    | checkov [options and args](https://www.checkov.io/2.Basics/CLI%20Command%20Reference.html) | `--directory .`      |

In addition to a textual report in the console, this job produces the following reports, kept for one day:

| Report         | Format                                                                       | Usage             |
| -------------- | ---------------------------------------------------------------------------- | ----------------- |
| `$TF_PROJECT_DIR/reports/checkov.xunit.xml` | [JUnit XML](https://www.checkov.io/8.Outputs/JUnit%20XML.html) | [GitLab integration](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsjunit) |
| `$TF_PROJECT_DIR/reports/checkov.native.json` | checkov JSON | [DefectDojo integration](https://defectdojo.github.io/django-DefectDojo/integrations/parsers/#checkov-report)<br/>_This report is generated only if DefectDojo template is detected_ |

You can skip checkov specific check adding following comment in code :

```terraform
resource "aws_s3_bucket" "foo-bucket" {
  region        = var.region
    #checkov:skip=CKV_AWS_20:The bucket is a public static content host
  bucket        = local.bucket_name
  force_destroy = true
  acl           = "public-read"
}
```

### `infracost` job

[Infracost](https://github.com/infracost/infracost) shows cloud cost estimates for infrastructure-as-code projects and uses the following variables:

| Name                   | description                   | default value         |
| ---------------------- | ----------------------------- | --------------------- |
| `TF_INFRACOST_ENABLED` | Set to `true` to enable infracost       | _none_ (disabled)     |
| `TF_INFRACOST_IMAGE`   | the infracost container image | `infracost/infracost` |
| `TF_INFRACOST_ARGS`    | infracost [CLI options and args](https://www.infracost.io/docs/#usage) | `breakdown`           |
| `TF_INFACOST_USAGE_FILE` | infracost [usage file](https://www.infracost.io/docs/usage_based_resources/#infracost-usage-file) | `infracost-usage.yml` |
| :lock: `INFRACOST_API_KEY`| the infracost API key | **required** |

To use infracost, an api key is needed.
To obtain it run :

```bash
docker run -it --name infracost infracost/infracost register
Please enter your name and email address to get an API key.
See our FAQ (https://www.infracost.io/docs/faq) for more details.
Name: Your Name
✔ Email: you_email@domain█

Thank you !
Your API key is: api_key
```

Save the API key as :lock: `INFRACOST_API_KEY` GitLab secret variable.

Set `INFRACOST_CURRENCY` variable to set currency [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes) prices should be converted to. Defaults to USD.

### `tf fmt` job

The terraform [`fmt` command](https://www.terraform.io/cli/commands/fmt) is usually used to format Terraform source files to a canonical format and style.

This job uses the _check_ mode, and fails if any file appears not being properly formatted. It uses the following variables:


| Name                 | description                                                                                | default value        |
| -------------------- | ------------------------------------------------------------------------------------------ | -------------------- |
| `TF_FMT_ENABLED`     | Set to `true` to enable terraform fmt                                                      | _none_ (disabled)    |
| `TF_FMT_ARGS`        | terraform fmt [options](https://www.terraform.io/cli/commands/fmt#usage)                   | `-diff -recursive`   |

## Variants

The Terraform template can be used in conjunction with template variants to cover specific cases.

### Vault variant

This variant allows delegating your secrets management to a [Vault](https://www.vaultproject.io/) server.

#### Configuration

In order to be able to communicate with the Vault server, the variant requires the additional configuration parameters:

| Name              | description                            | default value     |
| ----------------- | -------------------------------------- | ----------------- |
| `TBC_VAULT_IMAGE` | The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use (can be overridden) | `$CI_REGISTRY/to-be-continuous/tools/vault-secrets-provider:master` |
| `VAULT_BASE_URL`  | The Vault server base API url          | _none_ |
| :lock: `VAULT_ROLE_ID`   | The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID | **must be defined** |
| :lock: `VAULT_SECRET_ID` | The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID | **must be defined** |

#### Usage

Then you may retrieve any of your secret(s) from Vault using the following syntax:

```text
@url@http://vault-secrets-provider/api/secrets/{secret_path}?field={field}
```

With:

| Name                             | description                            |
| -------------------------------- | -------------------------------------- |
| `secret_path` (_path parameter_) | this is your secret location in the Vault server |
| `field` (_query parameter_)      | parameter to access a single basic field from the secret JSON payload |

#### Example

```yaml
include:
  # main template
  - project: 'to-be-continuous/terraform'
    ref: '3.4.0'
    file: '/templates/gitlab-ci-terraform.yml'
  # Vault variant
  - project: 'to-be-continuous/terraform'
    ref: '3.4.0'
    file: '/templates/gitlab-ci-terraform-vault.yml'

variables:
    # Secrets managed by Vault
    AWS_ACCESS_KEY_ID: "@url@http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/aws/prod/account?field=access_key_id"
    AWS_SECRET_ACCESS_KEY: "@url@http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/aws/prod/account?field=secret_access_key"
    VAULT_BASE_URL: "https://vault.acme.host/v1"
    # $VAULT_ROLE_ID and $VAULT_SECRET_ID defined as a secret CI/CD variable
```
