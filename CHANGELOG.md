# [3.4.0](https://gitlab.com/to-be-continuous/terraform/compare/3.3.0...3.4.0) (2022-12-30)


### Features

* **job:** add "needs" for Jobs with no dependency to make the CI faster ([e6cd115](https://gitlab.com/to-be-continuous/terraform/commit/e6cd11561ea1b0efc9bf91a5c9b8e77cbcf91e20))

# [3.3.0](https://gitlab.com/to-be-continuous/terraform/compare/3.2.1...3.3.0) (2022-12-13)


### Features

* **vault:** configurable Vault Secrets Provider image ([93c60c4](https://gitlab.com/to-be-continuous/terraform/commit/93c60c4376eec306fd90ea0f0a9047c2c79f94ba))

## [3.2.1](https://gitlab.com/to-be-continuous/terraform/compare/3.2.0...3.2.1) (2022-10-19)


### Bug Fixes

* -var and -var-file options cannot be used when applying a saved plan ([7a345de](https://gitlab.com/to-be-continuous/terraform/commit/7a345dea00c29c9cbef68e0a8057229ec3ee586d))

# [3.2.0](https://gitlab.com/to-be-continuous/terraform/compare/3.1.0...3.2.0) (2022-10-18)


### Features

* **job:** add terraform fmt checker ([08a1ee3](https://gitlab.com/to-be-continuous/terraform/commit/08a1ee3bf5eb86d46e5917ad4793fb87a249ec8a)), closes [#37](https://gitlab.com/to-be-continuous/terraform/issues/37)

# [3.1.0](https://gitlab.com/to-be-continuous/terraform/compare/3.0.0...3.1.0) (2022-10-04)


### Features

* normalize reports ([b2fe717](https://gitlab.com/to-be-continuous/terraform/commit/b2fe717285d39f066ef3d0d311dd47cb8b3c30b3))

# [3.0.0](https://gitlab.com/to-be-continuous/terraform/compare/2.8.0...3.0.0) (2022-08-05)


### Features

* adaptive pipeline ([735514e](https://gitlab.com/to-be-continuous/terraform/commit/735514e134f718fdd757b540d1ae83c2c69f0f9a))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [2.8.0](https://gitlab.com/to-be-continuous/terraform/compare/2.7.1...2.8.0) (2022-06-29)


### Features

* add implicit Backend configuration ([66cf555](https://gitlab.com/to-be-continuous/terraform/commit/66cf55570075d86c5b83a09bbee65f7dba679ea4))
* add implicit tfvars support ([eada2c7](https://gitlab.com/to-be-continuous/terraform/commit/eada2c7ba899b7083e3a2efadbbac7cfbb73b2cf))

## [2.7.1](https://gitlab.com/to-be-continuous/terraform/compare/2.7.0...2.7.1) (2022-05-25)


### Bug Fixes

* install curl when not found for DELETE method ([ac6ff96](https://gitlab.com/to-be-continuous/terraform/commit/ac6ff9651867b1ddd53add17ee006a5e756cf7e9))

# [2.7.0](https://gitlab.com/to-be-continuous/terraform/compare/2.6.0...2.7.0) (2022-05-13)


### Features

* remove GitLab managed Terraform state on environment deletion ([e4232e6](https://gitlab.com/to-be-continuous/terraform/commit/e4232e6412295cc2a8d9c75200277030b5c4b8cd))

# [2.6.0](https://gitlab.com/to-be-continuous/terraform/compare/2.5.0...2.6.0) (2022-05-09)


### Features

* add JSON format for TFSec output ([34c8f41](https://gitlab.com/to-be-continuous/terraform/commit/34c8f4139909125f13c6435e66eb66b811f7c21a))

# [2.5.0](https://gitlab.com/to-be-continuous/terraform/compare/2.4.3...2.5.0) (2022-05-01)


### Features

* configurable tracking image ([163c306](https://gitlab.com/to-be-continuous/terraform/commit/163c3069be2e3c23fa72961dfa318b7ceb47c4b8))

## [2.4.3](https://gitlab.com/to-be-continuous/terraform/compare/2.4.2...2.4.3) (2022-04-27)


### Bug Fixes

* avoid merge request pipelines ([4ecf4e5](https://gitlab.com/to-be-continuous/terraform/commit/4ecf4e5826e745dd6a2d401dd4c2a71a97ac1598))

## [2.4.2](https://gitlab.com/to-be-continuous/terraform/compare/2.4.1...2.4.2) (2022-02-24)


### Bug Fixes

* **vault:** revert Vault JWT authentication not working ([ef9078f](https://gitlab.com/to-be-continuous/terraform/commit/ef9078f350d40603a979367edf76d57c0b08f8ff))

## [2.4.1](https://gitlab.com/to-be-continuous/terraform/compare/2.4.0...2.4.1) (2022-02-23)


### Bug Fixes

* **vault:** Vault JWT authentication not working ([5732c06](https://gitlab.com/to-be-continuous/terraform/commit/5732c06f79873ba5e1fa3506dd4f29484891ba80))

# [2.4.0](https://gitlab.com/to-be-continuous/terraform/compare/2.3.2...2.4.0) (2022-01-10)


### Features

* Vault variant + non-blocking warning in case failed decoding [@url](https://gitlab.com/url)@ variable ([4f1265a](https://gitlab.com/to-be-continuous/terraform/commit/4f1265a39edd87fcac848778d2f687661e527421))

## [2.3.2](https://gitlab.com/to-be-continuous/terraform/compare/2.3.1...2.3.2) (2021-12-21)


### Bug Fixes

* **ci:** only launch production plan when merge request targets the prod branch ([898275e](https://gitlab.com/to-be-continuous/terraform/commit/898275e7fc5298621a34fc7ca2349ab7898befe2))

## [2.3.1](https://gitlab.com/to-be-continuous/terraform/compare/2.3.0...2.3.1) (2021-12-03)


### Bug Fixes

* execute hook scripts with shebang shell ([f552ee2](https://gitlab.com/to-be-continuous/terraform/commit/f552ee24d31d5d38cb68717008fe24bee0f374b7))

# [2.3.0](https://gitlab.com/to-be-continuous/terraform/compare/2.2.4...2.3.0) (2021-10-18)


### Features

* add plugin for cloud providers ([370c141](https://gitlab.com/to-be-continuous/terraform/commit/370c141bfeaab4cfa6d3bf7b179a2a5de2e2e8ad))

## [2.2.4](https://gitlab.com/to-be-continuous/terraform/compare/2.2.3...2.2.4) (2021-10-18)


### Bug Fixes

* revert test jobs on change only ([53378e4](https://gitlab.com/to-be-continuous/terraform/commit/53378e4e3b18f9d914e3c621d970a18b2a09edfb))

## [2.2.3](https://gitlab.com/to-be-continuous/terraform/compare/2.2.2...2.2.3) (2021-10-15)


### Bug Fixes

* update tflint image ([78014a7](https://gitlab.com/to-be-continuous/terraform/commit/78014a766ffc6543d6d2c71efa67b92e8f590eaf))

## [2.2.2](https://gitlab.com/to-be-continuous/terraform/compare/2.2.1...2.2.2) (2021-10-15)


### Bug Fixes

* enable change on subdir ([e80d07d](https://gitlab.com/to-be-continuous/terraform/commit/e80d07d39f14e91954c69568d26178b34aa2c6c3))

## [2.2.1](https://gitlab.com/to-be-continuous/terraform/compare/2.2.0...2.2.1) (2021-10-13)


### Bug Fixes

* only launch tests job on code update ([38b1143](https://gitlab.com/to-be-continuous/terraform/commit/38b114355805491bc03e7f12961775a086485808))

# [2.2.0](https://gitlab.com/to-be-continuous/terraform/compare/2.1.1...2.2.0) (2021-10-13)


### Features

* allow to override Terraform commands using a GitLab reference feature. ([64bed8a](https://gitlab.com/to-be-continuous/terraform/commit/64bed8a7973f133056542a131648b542479456e5))

## [2.1.1](https://gitlab.com/to-be-continuous/terraform/compare/2.1.0...2.1.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([fb622a1](https://gitlab.com/to-be-continuous/terraform/commit/fb622a1a34d5d77d4de4e472e77ebdb50977999d))

# [2.1.0](https://gitlab.com/to-be-continuous/terraform/compare/2.0.0...2.1.0) (2021-09-19)


### Features

* add infracost ([1ffba7f](https://gitlab.com/to-be-continuous/terraform/commit/1ffba7f5eb3679c8c314d6e9fb8a696f31525a09))

## [2.0.0](https://gitlab.com/to-be-continuous/terraform/compare/1.1.2...2.0.0) (2021-09-08)

### Features

* Change boolean variable behaviour ([f175aed](https://gitlab.com/to-be-continuous/terraform/commit/f175aed5f75a03802e70e7736c429c132e8ca565))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.1.2](https://gitlab.com/to-be-continuous/terraform/compare/1.1.1...1.1.2) (2021-07-21)

### Bug Fixes

* remove dependencies on tfsec, tflint and checkcov ([f8f1e87](https://gitlab.com/to-be-continuous/terraform/commit/f8f1e871574c8a14ae36002efe2d9a0288476ad0))

## [1.1.1](https://gitlab.com/to-be-continuous/terraform/compare/1.1.0...1.1.1) (2021-07-08)

### Bug Fixes

* conflict between vault and scoped vars ([27ebea7](https://gitlab.com/to-be-continuous/terraform/commit/27ebea71b1f872200ccf3f0d1d7c08f1fb9d8e3d))

## [1.1.0](https://gitlab.com/to-be-continuous/terraform/compare/1.0.0...1.1.0) (2021-06-10)

### Features

* move group ([869658c](https://gitlab.com/to-be-continuous/terraform/commit/869658cd53feb93b984028556114c318f872fc9b))

## 1.0.0 (2021-05-06)

### Features

* initial release ([9421d94](https://gitlab.com/Orange-OpenSource/tbc/terraform/commit/9421d94e6fd8149da03831dcda3723baaed7d745))
