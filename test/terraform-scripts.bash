#!/usr/bin/env bash

# load scripts from template
scripts=/tmp/tf-scripts.sh
echo "#!/bin/bash" > $scripts
sed -n '/BEGSCRIPT/,/ENDSCRIPT/p' "$(dirname ${BASH_SOURCE[0]})/../templates/gitlab-ci-terraform.yml" | sed 's/^  //' >> $scripts
source $scripts

# define terraform cli mock
function terraform() {
    nb_results=${#TF_RESULT[@]}
    if [[ ${nb_results} -ne 0 ]]
    then
        result=${TF_RESULT[0]}
        if [[ ${nb_results} -eq 1 ]]
        then
            TF_RESULT=()
        else
            TF_RESULT=${TF_RESULT[@]:1:$nb_results}
        fi
        echo "$result"
    else
        echo "terraform $*"
    fi
}

# export mock to make visible to child scripts
export -f terraform
